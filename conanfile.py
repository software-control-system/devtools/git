from conan import ConanFile
from conan.tools.system import package_manager
from conan.tools.gnu import AutotoolsDeps
from conan.tools.gnu import AutotoolsToolchain
from conan.tools.gnu import Autotools
from conan.tools.files import replace_in_file
from conan.errors import ConanInvalidConfiguration
import os

class GitRecipe(ConanFile):
    name = "git"

    settings = "os", "compiler", "build_type", "arch"

    requires = "libcurl/7.86.0", "zlib/1.2.13", "openssl/1.1.1t"

    build_requires = "libcurl/7.86.0"

    _autotools_args = [
        "NO_INSTALL_HARDLINKS=YesPlease",
        "RUNTIME_PREFIX=YesPlease",
        "gitexecdir=libexec/git-core",
        "template_dir=share/git-core/templates",
        "sysconfdir=etc",
        # "V=1"
    ]

    def validate(self):
        if self.settings.os not in ["Linux"]:
            raise ConanInvalidConfiguration("This recipe supports only Linux")

    def source(self):
        self.run("curl -s -L -O https://github.com/git/git/archive/refs/tags/v%s.tar.gz" % self.version)
        self.run("tar -xzf v%s.tar.gz --strip 1" % self.version)

    def generate(self):
        td = AutotoolsDeps(self)
        td.generate()
        tc = AutotoolsToolchain(self)
        tc.configure_args.append("--enable-pthreads=-pthread")
        tc.generate()

    def build(self):
        # Build configure
        self.run("make configure")

        autotools = Autotools(self)
        autotools.configure()
        replace_in_file(self, "Makefile", "EXTLIBS += -lrt", "EXTLIBS += -lrt -ldl")
        autotools.make(args=self._autotools_args)

    def package(self):
        autotools = Autotools(self)
        autotools.install(args=self._autotools_args)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
