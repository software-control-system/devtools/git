# Git

Pipeline to build Git 2.47.1 with conan on our legacy CentOS 6.

Git 2.47.1 for:
* Linux 64 bits: [git-2.47.1-linux-x86_64.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/git/-/jobs/artifacts/2.47.1/download?job=linux-x86_64)
* Linux 32 bits: [git-2.47.1-linux-i686.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/git/-/jobs/artifacts/2.47.1/download?job=linux-i686)
